import 'reflect-metadata'
import express from 'express'

const app = express()

app.all('*', (_, res) => {
  res.send('Hello world!')
})

app.listen('3000')
